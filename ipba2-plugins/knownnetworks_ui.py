# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'knownnetworks_ui.ui'
#
# Created: Thu Feb 21 12:01:59 2013
#      by: pyside-uic 0.2.14 running on PySide 1.1.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_KnownNetworks(object):
    def setupUi(self, KnownNetworks):
        KnownNetworks.setObjectName("KnownNetworks")
        KnownNetworks.resize(594, 524)
        self.horizontalLayout = QtGui.QHBoxLayout(KnownNetworks)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.networksTree = QtGui.QTreeWidget(KnownNetworks)
        self.networksTree.setMaximumSize(QtCore.QSize(200, 16777215))
        self.networksTree.setObjectName("networksTree")
        self.horizontalLayout.addWidget(self.networksTree)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtGui.QLabel(KnownNetworks)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label)
        self.label_2 = QtGui.QLabel(KnownNetworks)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_2)
        self.label_3 = QtGui.QLabel(KnownNetworks)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_3)
        self.label_4 = QtGui.QLabel(KnownNetworks)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_4)
        self.labelSSID = QtGui.QLineEdit(KnownNetworks)
        self.labelSSID.setReadOnly(True)
        self.labelSSID.setObjectName("labelSSID")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.labelSSID)
        self.labelBSSID = QtGui.QLineEdit(KnownNetworks)
        self.labelBSSID.setReadOnly(True)
        self.labelBSSID.setObjectName("labelBSSID")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.labelBSSID)
        self.labelLastJoined = QtGui.QLineEdit(KnownNetworks)
        self.labelLastJoined.setReadOnly(True)
        self.labelLastJoined.setObjectName("labelLastJoined")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.labelLastJoined)
        self.labelLastAutoJoined = QtGui.QLineEdit(KnownNetworks)
        self.labelLastAutoJoined.setReadOnly(True)
        self.labelLastAutoJoined.setObjectName("labelLastAutoJoined")
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.labelLastAutoJoined)
        self.verticalLayout.addLayout(self.formLayout)
        self.networkTree = QtGui.QTreeWidget(KnownNetworks)
        self.networkTree.setObjectName("networkTree")
        self.verticalLayout.addWidget(self.networkTree)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.horizontalLayout.setStretch(1, 1)

        self.retranslateUi(KnownNetworks)
        QtCore.QMetaObject.connectSlotsByName(KnownNetworks)

    def retranslateUi(self, KnownNetworks):
        KnownNetworks.setWindowTitle(QtGui.QApplication.translate("KnownNetworks", "Known Networks", None, QtGui.QApplication.UnicodeUTF8))
        self.networksTree.headerItem().setText(0, QtGui.QApplication.translate("KnownNetworks", "id", None, QtGui.QApplication.UnicodeUTF8))
        self.networksTree.headerItem().setText(1, QtGui.QApplication.translate("KnownNetworks", "SSID", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("KnownNetworks", "SSID", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("KnownNetworks", "BSSID", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("KnownNetworks", "Last joined", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("KnownNetworks", "Last auto joined", None, QtGui.QApplication.UnicodeUTF8))
        self.networkTree.headerItem().setText(0, QtGui.QApplication.translate("KnownNetworks", "Properties", None, QtGui.QApplication.UnicodeUTF8))

