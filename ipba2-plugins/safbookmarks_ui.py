# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'safbookmarks_ui.ui'
#
# Created: Thu Feb 21 12:01:58 2013
#      by: pyside-uic 0.2.14 running on PySide 1.1.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_SafBookmarks(object):
    def setupUi(self, SafBookmarks):
        SafBookmarks.setObjectName("SafBookmarks")
        SafBookmarks.resize(400, 300)
        self.verticalLayout = QtGui.QVBoxLayout(SafBookmarks)
        self.verticalLayout.setObjectName("verticalLayout")
        self.bookmarksTree = QtGui.QTreeWidget(SafBookmarks)
        self.bookmarksTree.setObjectName("bookmarksTree")
        self.verticalLayout.addWidget(self.bookmarksTree)
        self.label = QtGui.QLabel(SafBookmarks)
        self.label.setFrameShape(QtGui.QFrame.Panel)
        self.label.setFrameShadow(QtGui.QFrame.Raised)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)

        self.retranslateUi(SafBookmarks)
        QtCore.QMetaObject.connectSlotsByName(SafBookmarks)

    def retranslateUi(self, SafBookmarks):
        SafBookmarks.setWindowTitle(QtGui.QApplication.translate("SafBookmarks", "Safari Bookmarks", None, QtGui.QApplication.UnicodeUTF8))
        self.bookmarksTree.headerItem().setText(0, QtGui.QApplication.translate("SafBookmarks", "Bookmarks", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("SafBookmarks", "Right click on links to open in browser or copy", None, QtGui.QApplication.UnicodeUTF8))

